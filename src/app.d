import std.stdio : writeln, writefln, File;

alias constd = immutable double; //NOTE неизменяемый double

constd u = 3000; /// скорость истечения газа, [м/с]
constd dot_M = -300; /// скорость расхода топлива, [кг/с]
constd M_0 = 20000; /// начальная масса ракеты, [кг]
constd g = 9.8; /// константа, [м/с^2]
constd e = 2.71; /// константа, [единицы]
constd S = 1; /// площадь, на которую действует сила сопротивления воздуха, [м^2]
constd rho_0 = 0.8; /// плотность воздуха на космодроме, [кг/м^3]
constd k = 0.00014; /// коэффициент, [м^-1]

constd M = 5000; /// масса ракеты в момент измерения ее скорости и высоты, [кг]
constd t_0 = 0; /// точка отсчета времени, будем использовать для удобства чтения формул
constd t_1 = (M - M_0) / dot_M; /// время, за которое потрачено 15000 кг топлива, [с]
debug static assert(t_1 == 50.0); //NOTE проверка в debug-режиме на этапе компиляции

// под auto будем понимать математическую фукцию
// NOTE тип данных будет вычеслен на этапе компилляции и подставлен вместо auto
auto dot_v = (double t, double z_i, double v_i) => -u * dot_M / (
        M_0 + dot_M * t) - g - S * rho_0 * (e ^^ (-k * z_i)) * v_i * v_i / (M_0 + dot_M * t);
constd v_0 = 0; /// начальная скорость равна 0, [м/с]
constd z_0 = 0; /// начальное растояние равно 0, [м]

constd h = 0.1; /// шаг равномерной сетки
// NOTE cast(T) для приведения типов
immutable ulong n = cast(ulong)((t_1 - t_0) / h); /// количество шагов (количество итераций циткла)

double[n + 1] t;
double[n + 1] z;
double[n + 1] v;

void main()
{
    t[0] = 0;
    double tilde_z_i;
    double tilde_v_i;
    z[0] = z_0;
    v[0] = v_0;
    foreach (i; 0 .. n)
    {
        t[i + 1] = t_0 + (i + 1) * h; // равномерная сетка
        tilde_z_i = z[i] + h / 2 * v[i];
        tilde_v_i = v[i] + h / 2 * dot_v(t[i], z[i], v[i]);
        z[i + 1] = z[i] + h * tilde_v_i;
        v[i + 1] = v[i] + h * dot_v(t[i] + h / 2, tilde_z_i, tilde_v_i);
    }

    writefln("Израсходовав 15 тонн топлива, ракета будет находиться на высоте %.2f метров, двигаясь со скоростью %.2f метров в секунду.",
            z[n], v[n]);

    saveResult();
}

/// Функция сохраняет результаты вычисления (t[], z[], v[]) в CSV-файл
void saveResult()
{
    debug writefln("z: %(%f, %)", z[$ - 10 .. $]); //вывод среза массива из последних 10 элементов
    debug writefln("v: %(%f, %)", v[$ - 10 .. $]);

    auto file = File("result.csv", "w");
    file.writeln("t,z,v");
    for (int i = 0; i <= n; i++)
    {
        file.writefln("%(%f, %)", [t[i], z[i], v[i]]); //создаем массив и выводим строкой в файл
    }
}

// NOTE тесты будут выполняться, если запустить компилятор с ключом -unittest
unittest
{
    assert(n == 500);
}
